# CODE STYLE

The code style will be used as [PEP8] and [PEP7] adapted to the shell.

---

## Line length:

Length of the line should be 79 characters for code
and 72 characters for comments.


## Variables:

Variables with a single character, always will be without bracket characters
and multi-character variables will always be with bracket characters.

These should not be inside the double quotes, but should be next to them.

Examples:

    printf '%s %s\n' ${var1} $2


## Quotes, Strings and Integers:

Values of variable strings and values of command arguments as strings
(like path, text or similar), should enclosed by single quotes.

Values of variable integers and values of command arguments as integers
(numbers), should no enclosed by quotes.

Command arguments, function calls and executable calls directly
(as a command call or inside of command substitution,
not as a value of variable), should not enclosed by quotes.

Examples:

    command0 -x -gh -f '/dir1/dir2' -j 12 -p 'foobar'
    function -x 'bar' 59 ${var7} -w 'foo'${var8}
    use_var='command91 '$1' '$2' '$e' '${bar}
    ignore_vars='command91 $1 $2 $e ${bar}'
    cmdsub1=$(foocmd 783 $f ${var9} 'foo')


## "then", "do" and brackets statements:

The "then" and "do" statements should be always in below of the code line
with contains the "if", "for", "while" and "until"
and no extra indentation are applied, example:

    if [ -n ${var0} ]
    then
        if [ -n ${var1} ]
        then
            printf 'foo\n'
        fi
    fi

The "{" bracket should be always in below of the code line
with contain a function declaration and no extra indentation are applied, example:

    function_foo()
    {
        printf 'bar\n'
    }



[PEP7]: https://peps.python.org/pep-0007/
[PEP8]: https://peps.python.org/pep-0008/
