# CODE RULES

## Code:

The code is writen in POSIX Shell.
For more information, search for
"POSIX Shell", "Bashism" and "man 1p" in the internet.

The code lines do not have to be repeated,
they have to use conditional statements and functions.
This has to fulfill the [DRY] principle.

Keep the code simple and stupid, like the [KISS] principle.


## Variables:

In the shell, variables are always global scope,
except for parameters inside a file or function.

Unused variables should be freed with "unset" statement.


## Arrays:

Arrays will always be used by string variables, the IFS variable,
the "sed", "awk", and "grep" commands.


## Functions:

Each function declaration should be separated by file,
each file (with the suffix ".sh")
should have only one function declaration without any call outside of function
and each file (with the suffix ".run")
should have only one function call as an unique command call.


# Files:

In the first line of the files with the suffix ".run" and ".sh",
should be have "#!/bin/sh".

Files with the suffix ".run" should be executable (required by Laminar)
and should be located in the "jobs" directory.

Files with the suffix ".sh" should not be executable
and should be located in the "jobs/lib" directory.

Files (configuration files) with the suffix ".cfg" should not be executable
and should be located in the "jobs/cfg" directory.



[DRY]: https://en.wikipedia.org/wiki/Don%27t_repeat_yourself
[KISS]: https://en.wikipedia.org/wiki/KISS_principle
